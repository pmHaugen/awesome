#include <iostream>

int main()
{
	int size = 19;
	std::cout << "Triangle";
	for(int height = 0; height < size; height++)
	{
		for(int width = 0; width < height; width++)
		{
		if((height + width) == size)
		{
			std::cout << '#';
		}
		else
		{
			std::cout << '*';
		}
		}
		std::cout << std::endl;
	}
	return 0;
}
